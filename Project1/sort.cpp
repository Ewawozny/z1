#include <iostream>

#include "sort.h"

sort::sort(int b[], int s) {
	size = s;
	for (int i = 0; i < size; i++) {
		a[i] = b[i];
	}
}

sort::~sort() {}

void sort::print() {
	for (int i = 0; i < size; i++) {
		std::cout << a[i] << " ";
	}
}

void sort::insertSort() {
	for (int i, el, j = 1; j < size; j++) {
		el = a[j];
		i = j - 1;

		while (i >= 0 && a[i] > el) {
			a[i + 1] = a[i];
			i--;
		}

		a[i + 1] = el;
	}
}

bool sort::verify() {
	for (int i = 1; i < size; i++) {
		if (a[i] < a[i - 1]) return false;
	}
	return true;
}