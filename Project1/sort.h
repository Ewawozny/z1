#pragma once

#ifndef sort_h
#define sort_h

const int max_size = 256;

class sort {
private:
	int a[max_size];	// tablica do posortowania
	int size;			// rozmiar tablicy
public:
	sort(int b[] = { 0 }, int size = 1);	// konstruktor
	~sort();
	void print();	// wypisanie tablicy
	void insertSort();	// metoda sortujaca
	bool verify();	// metoda sprawdzajaca
};

#endif